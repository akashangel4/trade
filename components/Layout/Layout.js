import * as React from "react";
import Header from "../Header";
import Footer from "../Footer";
import CompanyLogo from "../../public/images/Logo_Wide_White.png";
const headerData = {
  logo: "Zolje",
  menuList: [
    { menuTitle: "Home", link: "/" },
    { menuTitle: "About", link: "#about" },
    { menuTitle: "Services", link: "#services" },
  ],
  buttonTitle: "Contact",
  buttonLink: "#contact",
};
const footerData = {
  footerLogo: "Zolje",
  copyright: "© 2022 ZOLJE, LLC. ALL RIGHTS RESERVED.",
  privacyPolicy:
    "<a href='#'>AML NOTICE</a> | <a href='#'>TERMS OF USE</a> | PRIVACY POLICY",
};

export const Layoutcomponent = ({ children }) => {
  return (
    <>
      <Header data={headerData} />
      <main>{children}</main>
      <Footer data={footerData} />
    </>
  );
};
