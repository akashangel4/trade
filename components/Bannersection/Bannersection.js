import React from "react";

export default function Bannersection(props) {
  return (
    <section className="banner-section">
      <div className="container">
        <div className="banner-content">
          {props?.data?.title && (
            <h1
              dangerouslySetInnerHTML={{
                __html: props?.data?.title,
              }}
            />
          )}
        </div>
      </div>
    </section>
  );
}
