import React, { useEffect } from "react";
import AOS from "aos";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChartLine } from "@fortawesome/free-solid-svg-icons";

export default function Cards({ data }) {
  useEffect(() => {
    AOS.init({ duration: 1000 });
    AOS.refresh();
  }, []);
  return (
    <>
      {data && (
        <>
          <div
            data-aos={data?.fadeSide == "left" ? "fade-left" : "fade-right"}
            data-aos-easing="ease-in-sine"
            className="card-container"
          >
            {data?.icon && (
              <div className="card-icon">
                {data?.icon == "growth" && (
                  <FontAwesomeIcon icon={faChartLine} />
                )}
              </div>
            )}
            {data?.cardTitle && (
              <h3 className="card-title">{data?.cardTitle}</h3>
            )}
            {data?.cardDescription && (
              <p className="card-description">{data?.cardDescription}</p>
            )}
            <div className="border-line"></div>
          </div>
        </>
      )}
    </>
  );
}
