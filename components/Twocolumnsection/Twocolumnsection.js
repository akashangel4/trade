import React from "react";
import Cards from "../Cards/Cards";

export default function Twocolumnsection(props) {
  return (
    <section className="two-cloumn-section" id="about">
      <div className="container">
        <div className="border-outline">
          {props?.data?.columnSection?.map((items, index) => {
            const bgImage = {
              backgroundImage: `url(${
                items?.image?.src ? items?.image?.src : ""
              })`,
            };
            return (
              <div
                className="md:grid md:grid-cols-12 column-container"
                key={index}
              >
                <div
                  className={
                    items?.imagePosition == "right"
                      ? "image-column md:col-span-5 order-2 right-image"
                      : "image-column md:col-span-5 order-1 left-image"
                  }
                  style={bgImage}
                >
                  <div className="image-column-content">
                    {items?.imageTitle && <h2>{items?.imageTitle}</h2>}
                    {items?.description && <p>{items?.description}</p>}
                  </div>
                </div>
                <div
                  className={
                    items?.imagePosition == "right"
                      ? "content-column md:col-span-7 order-1"
                      : "content-column md:col-span-7 order-2"
                  }
                >
                  <Cards data={items} />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
}
