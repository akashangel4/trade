import React from "react";
import Cards from "../Cards/Cards";

export default function Service(props) {
  return (
    <section
      className="service-section"
      id="services"
      style={{
        backgroundImage: `url(https://static.wixstatic.com/media/9b6a22_f18bf15f374b456fa0cdef0984a8c14c~mv2.jpg/v1/fill/w_1349,h_1023,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/9b6a22_f18bf15f374b456fa0cdef0984a8c14c~mv2.jpg)`,
      }}
    >
      <div className="container">
        <div className="card-section">
          {props?.data?.title && <h1>{props?.data?.title}</h1>}
          {props?.data?.columnItems && (
            <>
              <div className="md:grid md:grid-cols-2 gap-x-4">
                {props?.data?.columnItems?.map((items, index) => {
                  return <Cards data={items} key={index} />;
                })}
              </div>
            </>
          )}
        </div>
      </div>
    </section>
  );
}
