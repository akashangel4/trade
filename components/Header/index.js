import Image from "next/image";
import Link from "next/link";
import React, { useState } from "react";

export default function Header(props) {
  const [active, setActive] = useState();
  const handleClick = () => {
    setActive((current) => !current);
  };
  return (
    <section className="header">
      <div className="container">
        <div className="navbrand">
          {props?.data?.logo && (
            <Link href="/">
              {/* <Image src={props?.data?.logo} alt="Company Logo" /> */}
              <h2 className="samp-logo">{props?.data?.logo}</h2>
            </Link>
          )}
        </div>
        <nav className="navbar lg:block hidden">
          <ul>
            {props?.data?.menuList && (
              <>
                {props?.data?.menuList?.map((menuItems, index) => {
                  return (
                    <>
                      <li key={index}>
                        <Link href={menuItems?.link ? menuItems?.link : "#"}>
                          {menuItems?.menuTitle ? menuItems?.menuTitle : ""}
                        </Link>
                      </li>
                    </>
                  );
                })}
              </>
            )}
            {props?.data?.buttonTitle && (
              <li className="header-button">
                <Link
                  href={props?.data?.buttonLink ? props?.data?.buttonLink : "#"}
                >
                  {props?.data?.buttonTitle}
                </Link>
              </li>
            )}
          </ul>
        </nav>
        {/* Mobile size Navigation starts  */}
        <div className="mobile-navigation-menu block lg:hidden">
          <button
            className={active ? "hamburger-menu open" : "hamburger-menu"}
            onClick={handleClick}
          >
            <span>
              <div className="bar1"></div>
              <div className="bar2"></div>
              <div className="bar3"></div>
            </span>
          </button>
          <div
            className={
              active ? "navbar-collapse open" : "navbar-collapse close"
            }
          >
            <nav>
              <ul>
                {props?.data?.menuList && (
                  <>
                    {props?.data?.menuList?.map((menuItems, index) => {
                      return (
                        <>
                          <li key={index}>
                            <Link
                              href={menuItems?.link ? menuItems?.link : "#"}
                            >
                              {menuItems?.menuTitle ? menuItems?.menuTitle : ""}
                            </Link>
                          </li>
                        </>
                      );
                    })}
                  </>
                )}
                {props?.data?.buttonTitle && (
                  <li className="header-button">
                    <Link
                      href={
                        props?.data?.buttonLink ? props?.data?.buttonLink : "#"
                      }
                    >
                      {props?.data?.buttonTitle}
                    </Link>
                  </li>
                )}
              </ul>
            </nav>
          </div>
        </div>
        {/* Mobile size Navigation ends */}
      </div>
    </section>
  );
}
