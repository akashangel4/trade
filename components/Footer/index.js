import Image from "next/image";
import React from "react";

export default function Footer(props) {
  return (
    <div className="footer-section">
      <div className="container">
        {props?.data?.footerLogo && (
          // <Image src={props?.data?.footerLogo} alt="Footer logo" />
          <h2 className="samp-logo">{props?.data?.footerLogo}</h2>
        )}
        {props?.data?.copyright && (
          <p
            dangerouslySetInnerHTML={{
              __html: props?.data?.copyright,
            }}
          />
        )}
        {props?.data?.privacyPolicy && (
          <p
            dangerouslySetInnerHTML={{
              __html: props?.data?.privacyPolicy,
            }}
          />
        )}
      </div>
    </div>
  );
}
