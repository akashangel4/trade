import { Head, Html, Main, NextScript } from "next/document";
import React from "react";
export default function Document() {
  return (
    <Html>
      <Head>
        <title>Online Trading</title>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
        ></link>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link
          href="https://fonts.googleapis.com/css2?family=Barlow:wght@200;400&family=Open+Sans&display=swap"
          rel="stylesheet"
        />
        {/* AOS starts */}
        <link
          href="https://unpkg.com/aos@2.3.1/dist/aos.css"
          rel="stylesheet"
        />
        {/* AOS ends */}

        {/* Fav icon */}
        {/* <link
          data-cms-original-href="../public/images/facicon.png"
          href="../public/images/facicon.png"
          rel="icon"
          sizes="16x16"
          type="image/png"
        ></link> */}
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
