import React from "react";
import Bannersection from "../components/Bannersection/Bannersection";
// import Contact from "../components/Contact";
import { Layoutcomponent } from "../components/Layout/Layout";
import Service from "../components/Service";
import Twocolumnsection from "../components/Twocolumnsection/Twocolumnsection";
import column1Image from "../public/images/two-column-section/column1.jpg";
export default function Home() {
  const bannerData = {
    title: "Working to keep you<br>ahead of tomorrow.",
  };
  const twoColumnData = {
    columnSection: [
      {
        image: column1Image,
        imageTitle: "OUR STORY",
        description: "Where we are coming from",
        cardTitle: "Who We Are",
        cardDescription:
          "We are a private investment company that was created  to manage funds and portfolios of institutional and private clients worldwide.  Our founders designed the firm with a specific vision in mind: putting clients at the forefront and building an investment management firm to support ongoing growth of your investment objectives.",
        imagePosition: "left", // Choose right / left
      },
      {
        image: column1Image,
        icon: "growth",
        imageTitle: "OUR STORY",
        description: "Built for Growth",
        cardTitle: "Who We Are",
        cardDescription:
          "Our business model is based on working together with the most skilled specialists: third party marketers, external fund managers, legal advisors, fund administrators, accountants, and internal auditors. Utilizing the excellent relations built over the years with a network of law firms, auditors, and banks, Zolje has developed a solid competence in fund set-up including the selection of most suitable partners, organizational solutions and the negotiation of competitive pricing.",
        imagePosition: "right", // Choose right / left
      },
      {
        image: column1Image,
        imageTitle: "OUR STORY",
        description: "Where we are coming from",
        cardTitle: "Who We Are",
        cardDescription:
          "We are a private investment company that was created  to manage funds and portfolios of institutional and private clients worldwide.  Our founders designed the firm with a specific vision in mind: putting clients at the forefront and building an investment management firm to support ongoing growth of your investment objectives.",
        imagePosition: "left", // Choose right / left
      },
    ],
  };

  const serviceData = {
    title: "Services",
    columnItems: [
      {
        cardTitle: "Who We Are",
        cardDescription:
          "We are a private investment company that was created  to manage funds and portfolios of institutional and private clients worldwide.  Our founders designed the firm with a specific vision in mind: putting clients at the forefront and building an investment management firm to support ongoing growth of your investment objectives.",
        imagePosition: "", // Choose right / left
        fadeSide: "right", // Choose right / left for the content to come from
      },
      {
        cardTitle: "Who We Are2",
        cardDescription:
          "We are a private investment company that was created  to manage fund and portfolios of institutional and private clients worldwide.  Our founders designed the firm with a specific vision in mind: putting clients at the forefront and building an investment management firm to support ongoing growth of your investment objectives.",
        imagePosition: "", // Choose right / left
        fadeSide: "left", // Choose right / left for the content to come from
      },
    ],
  };
  const contactData = {
    email: "INFO@TRADIUMCAPITAL.COM",
    phone: "+1 (305) 204-6616",
  };
  return (
    <>
      <Layoutcomponent>
        <Bannersection data={bannerData} />
        <Twocolumnsection data={twoColumnData} />
        <Service data={serviceData} />
        {/* <Contact data={contactData} /> */}
      </Layoutcomponent>
    </>
  );
}
